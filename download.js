exports.run = async (client, message ,args, ops) =>{
  
  let ytdlcore = require('ytdl-core')
  //let ytdl = require('ytdl')
  let fs = require('fs')
  
  
  if (!args[0]) return message.channel.send('Il me faut le nom de la chanson!')
  
  //if (!args[0]) return message.channel.send("Entres une URL");
  let validate = await ytdlcore.validateURL(args[0]);

  if (!validate) {
    let commandFile = require("./searchdownload.js");
    return commandFile.run(client, message, args, ops);
  }
  
  let info = await ytdlcore.getInfo(args[0]);
  ytdlcore.downloadFromInfo(info)
  
  
const youtubedl = require('youtube-dl')
 
const video = youtubedl(args[0],
  // Optional arguments passed to youtube-dl.
  ['--format=18'],
  // Additional options can be given for calling `child_process.execFile()`.
  { cwd: __dirname })
 
// Will be called when the download starts.
video.on('infos', function(infos) {
  console.log('Download started')
  console.log('filename: ' + infos._filename)
  console.log('size: ' + infos.size)
})
 
video.pipe(fs.createWriteStream(`./downloads/${info.title}.mp3`))

}